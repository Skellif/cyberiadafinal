using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
	[System.NonSerialized] public float duration;
	[System.NonSerialized] public Entity owner;
	[System.NonSerialized] public float dmgMultiplier;
	public Rigidbody2D rb;
	public bool pierce;

	private bool flag = false;
	

	private void Start()
	{
		Destroy(this.gameObject, duration);
	}

	private void OnTriggerEnter2D(Collider2D collision)
	{
		

		if (!flag)
		{
			Entity hit = collision.gameObject.GetComponent<Entity>();
			if (hit != null)
			{
				owner.DealDmg(hit, dmgMultiplier);
				if (!pierce)
				{
					Destroy(this.gameObject);
				}

			}
		}
		flag = !pierce;
	}


}

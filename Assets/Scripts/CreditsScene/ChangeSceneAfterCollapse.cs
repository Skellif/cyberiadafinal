using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ChangeSceneAfterCollapse : MonoBehaviour
{
    [SerializeField] private float timeToCloseCollapse = 30;
    [SerializeField] private float timeSinceOpenScene;

    void Update()
    {
        if (timeToCloseCollapse < timeSinceOpenScene || Input.GetMouseButtonDown(0))
        {
            SceneManager.LoadScene(0);
        }
        timeSinceOpenScene += Time.deltaTime;
    }
}

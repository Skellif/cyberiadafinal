using UnityEngine;
using TMPro;


public class Dialogue : MonoBehaviour
{
    public TextMeshProUGUI nameText;
    public TextMeshProUGUI dialogueText;

    private DialogueScriptables currentScriptableQuest;
    private string currentNpcName;

    public WaveTimerBehavior WaveTimerBehavior;

    public void turnOnDialogueBox(DialogueScriptables currentDialogueToShow, string npcName)
    {
        currentScriptableQuest = currentDialogueToShow;
        currentNpcName = npcName;
        nameText.text = currentNpcName;
        dialogueText.text = currentDialogueToShow.DialogueText;
    }

    public void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (currentScriptableQuest.defaultDialogue != null)
            {
                turnOnDialogueBox(currentScriptableQuest.defaultDialogue, "Devil");
                return;
            }
            else if (currentScriptableQuest.optionalDialogue != null)
            {
                turnOnDialogueBox(currentScriptableQuest.optionalDialogue, "Hero");
                return;
            }
            else
            {
                closeDialogWindow();
                return;
            }
        }
    }
    private void closeDialogWindow()
    {
        Debug.Log("CLose");
        currentNpcName = "";
        currentScriptableQuest = null;
        WaveTimerBehavior.waveend = false;
        WaveTimerBehavior.isInDialogue = false;
        this.gameObject.SetActive(false);
    }
}
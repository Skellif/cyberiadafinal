using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "DialogueScriptables", menuName = "Dialogues/DialogueScriptables", order = 1)]
public class DialogueScriptables : ScriptableObject
{
    //dialogue system taken from Arkadiusz Oskar Kurylo Gihub - https://github.com/TakSeBiegam/OnlineRPG/blob/main/Assets/Scripts/NPCS/DialogueScriptables.cs
    [field: SerializeField]
    public string DialogueText { get; private set; }
    [field: SerializeField]
    public DialogueScriptables defaultDialogue = null;
    //Optional Dialogue is created when we wanna select one from a two options
    public DialogueScriptables optionalDialogue = null;
}
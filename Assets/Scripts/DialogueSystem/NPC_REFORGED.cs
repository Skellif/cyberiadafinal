using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPC_REFORGED : MonoBehaviour
{
    [SerializeField] private DialogueScriptables currentDialogue;
    [SerializeField] private ScriptableDialoguesManager DialogueManager;
    [SerializeField] GameObject dialogueCanvas;

    public bool hasTalked = false;
    public void triggerOnDialogueBox(DialogueScriptables currentDialogue)
    {
        dialogueCanvas.gameObject.GetComponent<Dialogue>().turnOnDialogueBox(currentDialogue, "Devil");
        dialogueCanvas.SetActive(true);
    }

}
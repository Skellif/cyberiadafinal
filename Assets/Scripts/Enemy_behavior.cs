using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_behavior : Entity
{
    [SerializeField] private float range;
    [SerializeField] private bool suicide;
    [SerializeField] private GameObject deathObject;
    [SerializeField] private Rigidbody2D rb;

    private Player player;
    private float timer = 0;

    void Start()
    {
        player = GameManager.Instance.player;
    }

    void Update()
    {
        timer += Time.deltaTime;
        if (player == null)
        {
            player = GameManager.Instance.player;
        }
        if (player != null)
        {
            Vector3 direction_to_player = (player.transform.position - transform.position);
            rb.MovePosition(rb.position + (Vector2)direction_to_player * speed * Time.fixedDeltaTime);

            if(timer >= attackSpeed)
            {
                if ((transform.position - player.transform.position).sqrMagnitude <= range * range)
                {
                    DealDmg(player);
                    timer = 0;
                    if (suicide)
                    {
                        Die();
                    }
                }
            }
            
        }
    }

    protected override void Die()
    {
        if (deathObject != null)
            Instantiate(deathObject, transform.position, transform.rotation);
        base.Die();
    }
}

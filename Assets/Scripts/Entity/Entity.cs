using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Entity : MonoBehaviour
{

    public float hp;
    public float maxHp;
    public float dmg;
    public float speed;
    public float attackSpeed;
    public float bulletVelocity;
    public float bulletduration;

    [SerializeField] protected Bullet bullet;
    [SerializeField] protected SoundManager soundManager;

    private void Awake()
    {
        hp = maxHp;
    }

    public virtual void ShootBullet(float velocityMultiplier = 1, float dmgMultiplier = 1, float angularOffset = 0, bool pierce = false, float durationMultiplier = 1)
    {
        // soundManager.PlaySound("shot");
        Bullet shotBullet = Instantiate(bullet, transform.position, transform.rotation * Quaternion.Euler(0, 0, angularOffset));
        shotBullet.owner = this;
        shotBullet.duration = bulletduration * durationMultiplier;
        shotBullet.rb.velocity = transform.up * bulletVelocity * velocityMultiplier;
        shotBullet.pierce = pierce;
    }

    public virtual void DealDmg(Entity e, float dmgMultiplier = 1)
    {
        e.TakeDmg(dmg * dmgMultiplier);
    }

    public virtual bool TakeDmg(float dmg)
    {
        // soundManager.PlaySound("takeDamage");
        hp -= dmg;
        if (hp > maxHp)
        {
            hp = maxHp;
            return false;
        }
        if (hp <= 0)
        {
            Die();
            return true;
        }
        return false;
    }

    protected virtual void Die()
    {
        // soundManager.PlaySound("death");
        Destroy(this.gameObject);
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : Entity
{
    public int Score { get; private set; }

    public Weapon weaponToPickUp = Weapon.Empty;
    public bool slowed = false;
    public float slowAmount = 1;


    [SerializeField] private float PlayerMovementSpeedX = 1;
    [SerializeField] private float PlayerMovementSpeedY = 1;
    [SerializeField] private Rigidbody2D PlayerRigidbody2D;
    [SerializeField] private Joystick_behavior movementJoystick;
    [SerializeField] private float arenaRadious;
    [SerializeField] private float lavaDmgSpeed;
    [SerializeField] private float lavaDmg;

    [SerializeField] private Weapon firstWeapon = Weapon.Pistol;
    [SerializeField] private Weapon secondWeapon = Weapon.Empty;


    [SerializeField] private float shotgunMaxAngle;
    [SerializeField] private float machingunMaxAttackSpeed;

    private bool usingFirstWeapon = true;
    private float timeStamp;
    private float lavaTimer;
    private Vector2 movement;

    private void Update()
    {

        if (transform.position.sqrMagnitude > arenaRadious * arenaRadious)
        {
            if (lavaTimer >= lavaDmgSpeed)
            {
                TakeDmg(lavaDmg);
                lavaTimer = 0;
            }

        }

        movement.x = PlayerMovementSpeedX * movementJoystick.joystickVec.x;
        movement.y = PlayerMovementSpeedY * movementJoystick.joystickVec.y;
        if (slowed)
        {
            movement.x *= slowAmount;
            movement.y *= slowAmount;
        }

        timeStamp += Time.deltaTime;
        lavaTimer += Time.deltaTime;
        slowed = false;
    }

    private void FixedUpdate()
    {
        PlayerRigidbody2D.MovePosition(PlayerRigidbody2D.position + movement * speed * Time.fixedDeltaTime);
    }

    private void Start()
    {
        timeStamp = attackSpeed;

        if (GameManager.Instance.player != null)
        {
            Debug.LogError("Player instance has been overriden");
        }
        GameManager.Instance.player = this;
    }

    public override void ShootBullet(float velocityMultiplier = 1, float dmgMultiplier = 1, float angularOffset = 0, bool pierce = false, float durationMultiplier = 1)
    {
        // soundManager.PlaySound("shot");
        Bullet shotBullet = Instantiate(bullet, transform.position, transform.rotation);
        shotBullet.owner = this;
        shotBullet.duration = bulletduration * durationMultiplier;
        shotBullet.rb.velocity = PlayerRigidbody2D.velocity + (Vector2)((Quaternion.Euler(0, 0, angularOffset) * movement) * bulletVelocity * velocityMultiplier);
        shotBullet.pierce = pierce;
        timeStamp = 0;
    }

    public void FireCurrentWeapon()
    {
        switch (usingFirstWeapon ? firstWeapon : secondWeapon)
        {
            case Weapon.Empty:
                Debug.LogError("Shooting empty!!!");
                break;
            case Weapon.Pistol:
                if (timeStamp > attackSpeed)
                {
                    ShootBullet();
                }

                break;
            case Weapon.Machinegun:
                if (timeStamp > attackSpeed * machingunMaxAttackSpeed)
                {
                    ShootBullet(2, machingunMaxAttackSpeed, 0, false, .15f);
                }
                break;
            case Weapon.Sword:
                if (timeStamp > attackSpeed)
                {
                    ShootBullet(0, 5f, 0, true, .01f);
                }
                break;
            case Weapon.Railgun:
                if (timeStamp > attackSpeed * 2)
                {
                    ShootBullet(5, 2, 0, true);
                }
                break;
            case Weapon.Shotgun:
                if (timeStamp > attackSpeed)
                {
                    ShootBullet(1, .5f);
                    ShootBullet(1, .5f, shotgunMaxAngle);
                    ShootBullet(1, .5f, -shotgunMaxAngle);
                    ShootBullet(1, .5f, (shotgunMaxAngle * .5f));
                    ShootBullet(1, .5f, -(shotgunMaxAngle * .5f));
                }
                break;
            default:
                Debug.LogError("Shooting default!!!");
                break;
        }
    }

    public void SwapWeapon()
    {
        if (usingFirstWeapon && secondWeapon != Weapon.Empty)
            usingFirstWeapon = false;
        else
            usingFirstWeapon = true;
    }

    public void PickupWeapon()
    {
        if (weaponToPickUp != Weapon.Empty)
        {
            if (secondWeapon == Weapon.Empty || !usingFirstWeapon)
            {
                secondWeapon = weaponToPickUp;
            }
            else
            {
                firstWeapon = weaponToPickUp;
            }
        }
    }

    public override void DealDmg(Entity e, float dmgMultiplier = 1)
    {
        if (e.TakeDmg(dmg * dmgMultiplier))
        {
            Score++;
        }
    }

    protected override void Die()
    {
        GameManager.Instance.GameOver(Score);
        base.Die();
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(Vector3.zero, arenaRadious);
    }
}

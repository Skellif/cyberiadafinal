using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FrostFog : MonoBehaviour
{
    [SerializeField] private float duration;
    [SerializeField] private float slow;
    [SerializeField] private float range;

    void Start()
    {
        Destroy(this.gameObject, duration);
    }

    void Update()
    {
        {
            if ((transform.position - GameManager.Instance.player.transform.position).sqrMagnitude <= range * range)
            {
                GameManager.Instance.player.slowAmount = slow;
                GameManager.Instance.player.slowed = true;
            }
        }
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(transform.position, range);
    }
}


using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;

    public float timer;

    public Player player;

    public enum LevelColor { White, Black, Red, Green};

    public LevelColor Color { get; private set; }

    private List<int> scores;

    private void Awake()
    {
        if(Instance != null)
        {
            Debug.LogError("Many instances of Game Manager");
        }
        Instance = this;

        timer = 0;

        scores = new List<int>();
    }

    private void Update()
    {
        timer += Time.deltaTime;
    }

    public void AddScore(int score)
    {
        scores.Add(score);
    }

    public List<int> ReadScores()
    {
        return scores;
    }
    public List<int> ReadScores(int count)
    {
        List<int> tmp = new List<int>();
        for (int i = scores.Count - count; i < scores.Count; i++)
        {
            tmp.Add(scores[i]);
        }
        return tmp;
    }

    public void ChangeColor(LevelColor color)
    {
        this.Color = color;
        timer = 0;
    }

    public void GameOver(int score) 
    {
        scores.Add(score);
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MM_ButtonManager : MonoBehaviour
{
    [SerializeField] private GameObject sureq;
    public void Play()
    {
        SceneManager.LoadScene(2);
    }

    public void Quit()
    {
        Application.Quit();
    }

    public void Credit()
    {
        SceneManager.LoadScene(1);
    }
}

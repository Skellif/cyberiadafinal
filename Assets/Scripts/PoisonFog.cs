using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoisonFog : MonoBehaviour
{
    [SerializeField] private float duration;
    [SerializeField] private float attackspeed;
    [SerializeField] private float dmg;
    [SerializeField] private float range;

    private float timer = 0;

    void Start()
    {
        Destroy(this.gameObject, duration);
    }

    void Update()
    {
        timer += Time.deltaTime;
        if(timer >= attackspeed)
        {
            if ((transform.position - GameManager.Instance.player.transform.position).sqrMagnitude <= range * range)
            {
                GameManager.Instance.player.TakeDmg(dmg);
            }
            timer = 0;
        }
        
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(transform.position, range);
    }
}

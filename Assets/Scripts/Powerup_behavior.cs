using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Powerup_behavior : MonoBehaviour
{
    public float livetime;
    public float maxlivetime;

    // Start is called before the first frame update
    void Start()
    {
        livetime = maxlivetime;
    }

    // Update is called once per frame
    void Update()
    {
        livetime -= Time.deltaTime;
        if(livetime <= 2)
        {
            //play animation of popup dying (attributes from animations)
        }
        if (livetime <= 0) Destroy(gameObject);
    }
}

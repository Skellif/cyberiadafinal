using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PropSpawner : MonoBehaviour
{
    public GameObject[] prop;

    public float awaittimemax;
    public WaveTimerBehavior WaveTimerObject;
    public bool isenemyspawner;
    public int maxboundarylocation;

    int chosenitem;
    float timecurrent = 0;


    void Update()
    {
        if (!WaveTimerObject.waveend)
        {
            timecurrent += Time.deltaTime;
            if (timecurrent >= awaittimemax)
            {
                if (isenemyspawner)
                    chosenitem = WaveTimerObject.wavenumber;

                else chosenitem = 0;

                float radious = Random.Range(-maxboundarylocation, maxboundarylocation);
                Vector2 point = RandomTools.RandomPointOnCircle(radious);
                GameObject temp_go = Instantiate(prop[chosenitem], point, Quaternion.identity);
                timecurrent = 0;
            }
        }

    }
}

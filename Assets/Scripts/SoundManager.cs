using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    [System.Serializable] private class MusicNamePair
    {
        public string name;
        public AudioClip clip;
    }

    [SerializeField] private AudioSource source;
    [SerializeField] private List<MusicNamePair> sounds;

    public void PlaySound(string name)
    {
        StopSound();
        foreach(MusicNamePair pair in sounds)
        {
            if(pair.name == name)
            {
                source.clip = pair.clip;
                break;
            }
        }
        source.Play();
    }

    public void StopSound()
    {
        source.Stop();
        source.clip = null;
    }

}

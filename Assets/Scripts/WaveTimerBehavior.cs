using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveTimerBehavior : MonoBehaviour
{

    [SerializeField] private float currentwavetime;
    [SerializeField] private DialogueScriptables[] startingDialogues;

    public int[] wavetime;

    public int wavenumber;

    public int maxwavenumber;

    public bool waveend;

    public bool isInDialogue;
    private bool setKinematicToEnabledMode;

    [SerializeField] private GameObject dialogue_menager;

    public GameObject player;

    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        currentwavetime = 0;
        wavenumber = 0;
        waveend = true;
        isInDialogue = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (!waveend)
        {
            if (setKinematicToEnabledMode)
            {
                player.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.None;
                setKinematicToEnabledMode = false;
            }
            currentwavetime += Time.fixedDeltaTime;
            if (currentwavetime >= wavetime[wavenumber])
            {
                currentwavetime = 0;
                if (wavenumber <= maxwavenumber + 1)
                {
                    wavenumber++;
                    currentwavetime = 0;
                    waveend = true;
                }
                else
                {
                    //end game
                }
            }
        }
        else
        {
            if (!isInDialogue)
            {
                GameObject[] gos = GameObject.FindGameObjectsWithTag("Enemy");
                foreach (GameObject go in gos)
                    Destroy(go);
                setKinematicToEnabledMode = true;
                isInDialogue = true;
                player.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeAll;
                switch (wavenumber)
                {
                    case 0:
                        {
                            player.GetComponent<SpriteRenderer>().color = new Color(1f, 0, 0);
                            dialogue_menager.GetComponent<NPC_REFORGED>().triggerOnDialogueBox(startingDialogues[0]);
                            break;
                        }
                    case 1:
                        {
                            player.GetComponent<SpriteRenderer>().color = new Color(0, 1f, 0.1f);
                            dialogue_menager.GetComponent<NPC_REFORGED>().triggerOnDialogueBox(startingDialogues[1]);
                            break;
                        }
                    case 2:
                        {
                            player.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 0);
                            dialogue_menager.GetComponent<NPC_REFORGED>().triggerOnDialogueBox(startingDialogues[2]);
                            break;
                        }
                    case 3:
                        {
                            player.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f);
                            dialogue_menager.GetComponent<NPC_REFORGED>().triggerOnDialogueBox(startingDialogues[3]);
                            break;
                        }
                }
            }
        }
    }
}

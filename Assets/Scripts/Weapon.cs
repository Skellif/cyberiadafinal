using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Weapon { Empty, Pistol, Sword, Machinegun, Shotgun, Railgun }

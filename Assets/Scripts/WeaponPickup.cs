using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponPickup : MonoBehaviour
{
    [SerializeField] private float weaponPickUpRadious;
    [SerializeField] private Weapon weaponToPickUp = Weapon.Empty;

    private void Update()
    {
        if((transform.position - GameManager.Instance.player.transform.position).sqrMagnitude <= weaponPickUpRadious * weaponPickUpRadious)
        {
            GameManager.Instance.player.weaponToPickUp = weaponToPickUp;
        }
        else
        {
            GameManager.Instance.player.weaponToPickUp = Weapon.Empty;
        }
    }
}
